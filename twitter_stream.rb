require 'twitter'
require 'timeout'
require 'csv'
require 'yaml'

class TwitterStream

  attr_accessor :client, :tweets, :words, :stop_words 
  
  def initialize
    client_config ||= { consumer_key: ENV['consumer_key'],
                        consumer_secret: ENV['consumer_secret'],
                        access_token: ENV['access_token'],
                        access_token_secret: ENV['access_token_secret']
                      }

    @client = Twitter::Streaming::Client.new(client_config)
    @tweets = []
    @words = Hash.new(0)
    @stop_words = []
  end

  def run(duration = 5, count = 10)
    puts "Populating stop words in memory."
    populate_stop_words
    duration_in_secs = duration * 60
    puts "Starting to read tweets & creating word list"
    t1 =  Thread.new do
            begin
              Thread.handle_interrupt(Timeout::Error => :on_blocking) {
                # using timeout libray to read tweets for 'duration' of seconds
                timeout(duration_in_secs) {
                  read_tweets
                }
              }
            rescue Timeout::Error => e 
              #do nothing
            end
          end
    t1.join
  end

  def get_results(count, duration)
    
    puts "Top #{count} tweeted words in last #{duration} mins:"
    rank_words.first(count).each_with_index do |word, index|
       puts "#{index + 1}. #{word[0]} - #{word[1]}"
    end
  end
  
  def serialize_words
    File.open("words.yml", "w") do |file|
      file.write words.to_yaml
    end
    File.open("status.txt", "w"){ |file| file.puts 'dirty' }
  end
  
  def deserialize_words
    if File.open("status.txt", "r"){ |file| file.read } == 'dirty'
      words = YAML::load_file "words.yml"
    end
    File.open("status.txt", "w"){ |file| file.puts 'clean' }
  end

  private
    def clean_tweets(text)
      text.gsub(/[^\w+ '@#\/:.-\\]/, '').downcase
    end

    def read_tweets
      client.sample do |object|
        case object
        when Twitter::Tweet
          if (object.text.size > 0 && object.user.lang == "en")
            t = clean_tweets(object.text)
            add_tweet_to_word_dict(t)
          end
        when Twitter::Streaming::StallWarning
          warn "Falling behind!"
        end
      end
    end

    def populate_stop_words
      File.open('stop-words_english_6_en.txt') do |f|
        f.each_line do |line|
          stop_words << line.strip
        end
      end
    end

    def add_tweet_to_word_dict(tweet)
      wat = remove_stop_words(tweet.split(' '))
      wat.each { |word|
        words[word] += 1
      }
    end

    def remove_stop_words(tweet)
      tweet.delete_if { |wrd| stop_words.include?(wrd) }
    end

    def rank_words
      words.sort_by {|key, value| value}.reverse
    end
    
end

begin
  ts = TwitterStream.new
  duration = 5
  count = 10
  ts.deserialize_words
  ts.run(duration)
  ts.get_results(count, duration)
rescue Interrupt => e
  puts "Interupted, adding words read to file."
  ts.serialize_words
rescue => e
  puts e.message
  puts e.backtrace
ensure
  puts "done!"
end
